
<!DOCTYPE html>
<html>

    <head>
        <title>ADD</title>
        <meta charset="UTF-8" name="news portal" content="news from the world">
        <link rel="stylesheet" href="style.css" type="text/css">
        <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:ital,wght@1,700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Chelsea+Market&family=Sen&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">

        <style>
            section{
                height: inherit;
                padding: 2%;
                width: 100%;
                margin: 0 auto;
            }
        </style>

    </head>

    <body>
        
        <header>

            <div id="ikone">
                <i class="fa fa-facebook-square fa-2x"></i>
                <i class="fa fa-twitter-square fa-2x"></i>
                <i class="fa fa-instagram fa-2x"></i>
                <i class="fa fa-youtube-square fa-2x"></i>
                <i class="fa fa-envelope fa-2x"></i>
            </div>
            
            <div id="logo">
               <div id="logo2">
                   <h5>PORTAL</h5>
               </div>
               <div id="logo3">
                    <p> NEWS PORTAL</p>
               </div>
            </div>

            <nav>
                <ul>
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="kategorija.php?kategorija=travels">TRAVELS</a></li>
                    <li><a href="kategorija.php?kategorija=culture">CULTURE</a></li>
                    <li><a href="unos.php">ADD</a></li>
                    <li><a href="administracija.php">ADMINISTRATION</a></li>
                </ul>
            </nav>
            <div style="clear: both;"></div>
        </header>

        <main>

            <section>

                <form action="skripta.php"  method="POST" name="unos" enctype="multipart/form-data">
                    <div class="form-item">
                        <label for="title">Title:</label>
                        <div class="form-field">
                            <input type="text" id="title" name="title" size="58" autofocus required>
                        </div>
                        <span id="porukaTitle" class="bojaPoruke"></span>
                    </div>
                    <div class="form-item">
                        <label for="opis">Brief content:</label>
                        <div class="form-field">
                            <textarea name="about" id="about" cols="60" rows="10" required ></textarea>
                        </div>
                        <span id="porukaAbout" class="bojaPoruke"></span>
                    </div>
                    <div class="form-item">
                        <label for="content">Main content:</label>
                        <div class="form-field">
                            <textarea name="content" id="content" cols="60" rows="20" required></textarea>
                        </div>
                        <span id="porukaContent" class="bojaPoruke"></span>
                    </div>
                    <div class="form-item">
                        <label for="photo">Picture:</label>
                        <div class="form-field">
                            <input type="file" accept="image/jpg,image/gif" name="photo" id="photo" >
                        </div>
                        <span id="porukaPhoto" class="bojaPoruke"></span>
                    </div>
                    <div class="form-item">
                        <label for="category">Category:</label>
                        <div class="form-field">
                            <select name="category" id="category" required>
                                <option value="" disabled selected>Select category</option>
                                <option value="travels">Travels</option>
                                <option value="culture">Culture</option>
                            </select>
                        </div>
                        <span id="porukaCategory" class="bojaPoruke"></span>
                    </div>
                    <div class="form-item">
                        <label>Save to archive:
                            <div class="form-field">
                                <input type="checkbox" name="archive">
                            </div>
                        </label>
                    </div>
                    <div class="form-item">
                            <button type="submit" name="submit" id="submit" class="send"><span>Submit</span> </button>
                            <button type="reset" name="reset" id="reset"> <span> Reset </span></button>
                    </div>

                </form>
            </section>

        </main>

    <script type="text/javascript"> 
 
            // Provjera forme prije slanja 
            document.getElementById("submit").onclick = function(event) {                                     
                var slanjeForme = true;                                     
                // Naslov vjesti (5-30 znakova)                   
                var poljeTitle = document.getElementById("title");                   
                var title = document.getElementById("title").value;                   
                if (title.length < 5 || title.length > 30) {                      
                    slanjeForme = false;                      
                    poljeTitle.style.border="3px solid red";                      
                    document.getElementById("porukaTitle").innerHTML="Naslov vjesti mora imati između 5 i 30 znakova!<br>";                  
                    } else {                      
                        poljeTitle.style.border="3px solid  rgba(23, 175, 23, 0.959)";                      
                        document.getElementById("porukaTitle").innerHTML="";                  
                        }                                     
                // Kratki sadržaj (10-100 znakova)                  
                var poljeAbout = document.getElementById("about");                  
                var about = document.getElementById("about").value;                   
                if (about.length < 10 || about.length > 100) {                     
                        slanjeForme = false;                      
                        poljeAbout.style.border="3px solid red";                     
                        document.getElementById("porukaAbout").innerHTML="Kratki sadržaj mora imati između 10 i 100 znakova!<br>";                  
                        } else {                      
                            poljeAbout.style.border="3px solid  rgba(23, 175, 23, 0.959)";                      
                            document.getElementById("porukaAbout").innerHTML="";                   
                        } 

                // Sadržaj mora biti unesen                   
                var poljeContent = document.getElementById("content");                   
                var content = document.getElementById("content").value;                   
                if (content.length == 0) {                      
                    slanjeForme = false;                      
                    poljeContent.style.border="3px solid red";                      
                    document.getElementById("porukaContent").innerHTML="Sadržaj mora biti unesen!<br>";                  
                 } else {                      
                     poljeContent.style.border="3px solid  rgba(23, 175, 23, 0.959)"; 
                      document.getElementById("porukaContent").innerHTML="";                   
                    } 

                // Slika mora biti unesena                   
                var poljeSlika = document.getElementById("photo");                   
                var pphoto = document.getElementById("photo").value;                   
                if (pphoto.length == 0) {                      
                    slanjeForme = false;                      
                    poljeSlika.style.border="3px solid red";                     
                    document.getElementById("porukaPhoto").innerHTML="Slika mora biti unesena!<br>";                   
                } else {                      
                    poljeSlika.style.border="3px solid  rgba(23, 175, 23, 0.959)";                      
                    document.getElementById("porukaPhoto").innerHTML="";                   
                }                   
                // Kategorija mora biti odabrana                   
                var poljeCategory = document.getElementById("category");                   
                if(document.getElementById("category").selectedIndex == 0) {                      
                    slanjeForme = false;                      
                    poljeCategory.style.border="3px solid red";                     
                    document.getElementById("porukaCategory").innerHTML="Kategorija mora biti odabrana!<br>";                   
                } else {                      
                    poljeCategory.style.border="3px solid  rgba(23, 175, 23, 0.959)";                      
                    document.getElementById("porukaCategory").innerHTML="";                   
                }                   

                if (slanjeForme != true) {                     
                    event.preventDefault();                  
                }                                  
                   };            
        </script>   

        <footer>
            <div id="podnozje">
            <p> Copyright 2020 NEWS PORTAL </p>
                <!--<p>kvidakovi@tvz.hr</p>-->
            </div>
        </footer>

    </body>
</html>

 
 
 