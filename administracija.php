<?php
session_start();
include 'connect.php';
define ('UPLPATH', 'img/');

$uspjesnaPrijava=false;

// Provjera da li je korisnik došao s login forme 
if (isset($_POST['prijava'])) { 

    // Provjera da li korisnik postoji u bazi uz zaštitu od SQL injectiona    
    $prijavaImeKorisnika = $_POST['username'];    
    $prijavaLozinkaKorisnika = $_POST['pass']; 
 
    $sql = "SELECT korisnicko_ime, lozinka, razina FROM korisnik         
            WHERE korisnicko_ime = ?";    

    $stmt = mysqli_stmt_init($dbc);   

    if (mysqli_stmt_prepare($stmt, $sql)) {       
        mysqli_stmt_bind_param($stmt, 's', $prijavaImeKorisnika);         
        mysqli_stmt_execute($stmt);         
        mysqli_stmt_store_result($stmt);      
    }    

    mysqli_stmt_bind_result($stmt, $imeKorisnika, $lozinkaKorisnika, $levelKorisnika);    
    mysqli_stmt_fetch($stmt); 
 
   //Provjera lozinke    
   if (password_verify($_POST['pass'], $lozinkaKorisnika) && mysqli_stmt_num_rows($stmt) > 0) {       
       $uspjesnaPrijava = true; 
 
      // Provjera da li je admin       
      if($levelKorisnika == 1) {          
          $admin = true;       
        }       
      else {          
          $admin = false;       
        }       
        
        //postavljanje session varijabli       
        $_SESSION['$username'] = $imeKorisnika;       
        $_SESSION['$level'] = $levelKorisnika;    
    } 
    
    else {      
         $uspjesnaPrijava = false;    
        }     
    }

    if (isset($_POST['delete'])){
        $id=$_POST['id'];
        $query="DELETE FROM portal WHERE id=$id";
        $result=mysqli_query($dbc, $query);
    }

    if(isset($_POST['update'])){
        $picture = $_FILES['photo']['name'];
        $title=$_POST['title'];
        $about=$_POST['about'];
        $content=$_POST['content'];
        $category=$_POST['category'];
        $id=$_POST['id'];
        if(isset($_POST['archive'])){
            $archive=1;
         }
         else{
            $archive=0;
         }

        $target_dir = 'img/' . $picture;
        move_uploaded_file($_FILES["photo"]["tmp_name"], $target_dir); 
 
        $query = "UPDATE portal
                  SET naslov='$title', sazetak='$about', tekst='$content',
                  slika='$picture', kategorija='$category', arhiva=$archive
                  WHERE id=$id"; 
                  $result = mysqli_query($dbc, $query);                 
        }
?>

<!DOCTYPE html>
<html>

    <head>

        <title>ADMINISTRATION</title>
        <meta charset="UTF-8" name="news portal" content="news from the world">
        <link rel="stylesheet" href="style.css" type="text/css">
        <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:ital,wght@1,700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Chelsea+Market&family=Sen&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">

        <style>
            section{
                height: inherit;
                padding: 2%;
                width: 100%;
                margin: 0 auto;
            }
        </style> 

    </head>

    <body>
        
        <header>

            <div id="ikone">
                <i class="fa fa-facebook-square fa-2x"></i>
                <i class="fa fa-twitter-square fa-2x"></i>
                <i class="fa fa-instagram fa-2x"></i>
                <i class="fa fa-youtube-square fa-2x"></i>
                <i class="fa fa-envelope fa-2x"></i>
            </div>

            <div id="logo">
               <div id="logo2">
                   <h5>PORTAL</h5>
               </div>
               <div id="logo3">
                    <p> NEWS PORTAL</p>
               </div>
            </div>

            <nav>
                <ul>
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="kategorija.php?kategorija=travels">TRAVELS</a></li>
                    <li><a href="kategorija.php?kategorija=culture">CULTURE</a></li>
                    <li><a href="unos.php">ADD</a></li>
                    <li><a href="administracija.php">ADMINISTRATION</a></li>
                </ul>
            </nav>
            <div style="clear: both;"></div>
        </header>

        <main>

            <section>
                <?php     
                // Pokaži stranicu ukoliko je korisnik uspješno prijavljen i administrator je          
                    if (($uspjesnaPrijava == true && $admin == true) || (isset($_SESSION['$username'])) && $_SESSION['$level'] == 1) { 

                        $query="SELECT * FROM portal";
                        $result=mysqli_query($dbc, $query);
        
                        while($row=mysqli_fetch_array($result)){
                        
                            print'
                            <form action=""  method="POST" name="unos" enctype="multipart/form-data">
                                <div class="form-item">
                                    <label for="title">Title:</label>
                                    <div class="form-field">
                                        <input type="text" id="title" name="title" size="58" value ="' . $row['naslov'] . '"autofocus>
                                    </div>
                                </div>
                                <div class="form-item">
                                    <label for="opis">Brief content:</label>
                                    <div class="form-field">
                                        <textarea name="about" id="about" cols="60" rows="10">' . $row['sazetak'] . '</textarea>
                                    </div>
                                </div>
                                <div class="form-item">
                                    <label for="content">Main content:</label>
                                    <div class="form-field">
                                        <textarea name="content" id="content" cols="60" rows="20">' . $row['tekst'] . '</textarea>
                                    </div>
                                </div>
                                <div class="form-item">
                                    <label for="photo">Picture:</label>
                                    <div class="form-field">
                                        <input type="file" accept="image/jpg,image/gif" name="photo" id="photo" value="' . $row['slika'] . '"> <br>
                                        <img src="' . UPLPATH . $row['slika'] . '" width="100">
                                    </div>
                                </div>
                                <div class="form-item">
                                    <label for="category">Category:</label>
                                    <div class="form-field">
                                        <select name="category" id="category" value="' . $row['kategorija'] . '">
                                            <option value="travels">Travels</option>
                                            <option value="culture">Culture</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-item">
                                    <label>Save to archive:
                                        <div class="form-field">';
                                            if($row['arhiva']==0){
                                                echo '<input type="checkbox" name="archive" id="archive" />';
                                            }
                                            else{
                                                echo '<input type="checkbox" name="archive" id="archive" checked/>';
                                            }
                                        echo '</div>
                                    </label>
                                </div>
                                <div class="form-item">
                                    <input type="hidden" name="id" value="' . $row['id'] . '"> 
                                    <button type="submit" name="update" id="update" value="Update"><span>Update</span></button>
                                    <button type="reset" name="reset" id="reset2" value="Reset" ><span>Reset</span></button>
                                    <button type="submit" name="delete" id="delete" value="Delete" ><span>Delete</span></button>
                                </div>
        
                            </form>';
                        }
                       
                    }            
                        
                        // Pokaži poruku da je korisnik uspješno prijavljen, ali nije administrator          
                     else if ($uspjesnaPrijava == true && $admin == false) {                          
                        echo '<p class="login_msg">Hello ' . $imeKorisnika . '!  You are successfully logged in but you are not an administrator.</p>'; 
                        session_destroy();
                    } else if($uspjesnaPrijava == false && !empty($_POST['username']) && !empty($_POST['pass'])){

                        echo "<p class='login_msg'>User doesn't exists. Please fill the <br> <a href='registracija.php'>Registration form</a></p>";                                 
                    } 
                    else if ($uspjesnaPrijava == false && !(isset($_SESSION['$username'])) && !(isset($_SESSION['$pass']))) {             
                ?>                
                            <!-- Forma za prijavu --> 
                            <section role="main" id="prijava">              
                            <form enctype="multipart/form-data" action="" method="POST">                  
                                <div class="form-item">                     
                                    <span id="porukaUsername" class="bojaPoruke"></span>                     
                                    <label for="title">Username: </label>                     
                                    <div class="form-field">                     
                                        <input type="text" name="username" id="username" class="formfield-textual">                 
                                    </div>                 
                                </div>                                 
                                <div class="form-item">                     
                                    <span id="porukaPass" class="bojaPoruke"></span>                     
                                    <label for="pass">Password: </label>                     
                                    <div class="form-field"> 
                                        <input type="password" name="pass" id="pass" class="formfield-textual">                  
                                    </div>                  
                                </div>                                                  
                                <div class="form-item">                     
                                    <button type="submit" value="Prijava" name="prijava" id="slanje" onclick="validacija();"><span>Submit</span></button>                 
                                </div> 
                            </form>     
                            </section> 
                    <?php 
                        }
                     ?> 

            </section>                   
        </main>
        <script type="text/javascript"> 
            //javascript validacija forme   
            function validacija (event){
                var slanjeForme = true;                                     
                        
                // Korisničko ime mora biti uneseno                 
                var poljeUsername = document.getElementById("username");                 
                var username = document.getElementById("username").value;                 
                
                if (username.length == 0) {                     
                    slanjeForme = false;                     
                    poljeUsername.style.border="1px solid red";                     
                    document.getElementById("porukaUsername").innerHTML="<br>Username is missing<br>";                 
                    } else {                     
                        poljeUsername.style.border="1px solid green";                     
                        document.getElementById("porukaUsername").innerHTML="";                 
                        }                     
                        
                //Lozinka mora biti postavljena              
                var poljePass = document.getElementById("pass");                
                var pass = document.getElementById("pass").value;                               
                
                if (pass.length == 0) {                     
                    slanjeForme = false;                     
                    poljePass.style.border="1px solid red";                                       
                    document.getElementById("porukaPass").innerHTML="<br>Password is missing<br>";                     
                                    
                    } else {                     
                        poljePass.style.border="1px solid green";                                         
                        document.getElementById("porukaPass").innerHTML="";                     
                                       
                        }                      
                        
                if (slanjeForme != true) {                     
                    event.preventDefault();                 
                    } 

            };              

        </script>  
        
    <footer>
        <div id="podnozje">
        <p> Copyright 2020 NEWS PORTAL </p>
                <!--<p>kvidakovi@tvz.hr</p>-->
        </div>
    </footer>

</body>
</html>

       

