<?php
include "connect.php";
define ('UPLPATH', 'img/');

    if (isset($_POST['submit'])){
        $title=$_POST['title'];
        $about=$_POST['about'];
        $content=$_POST['content'];
        $picture= $_FILES['photo']['name'];
        $category=$_POST['category'];
        $date=date('Y-m-d');
        $fdate=date('d.m.Y');
        if(isset($_POST['archive'])){     
            $archive=1;
        }
        else{     
            $archive=0; 
            } 
        
        $target_dir = 'img/' . $picture;
        move_uploaded_file($_FILES["photo"]["tmp_name"], $target_dir); 
        
        $query = "INSERT INTO portal (datum, naslov, sazetak, tekst, slika, kategorija, arhiva ) VALUES ('$date', '$title', '$about', '$content', '$picture', '$category', '$archive')"; 
        
        $result = mysqli_query($dbc, $query) or die('Error querying databese.'); 
        mysqli_close($dbc); 
    }

    ?>

    <!DOCTYPE html>
    <html>

        <head>

            <title>ARTICLE</title>
            <meta charset="UTF-8" name="news portal" content="news from the world">
            <link rel="stylesheet" href="style.css" type="text/css">
        
            <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">

            <style>

                body{
                    background-color: red;
                    min-height:100%;
                }

               main{
               }

                section{
                    height: inherit;
                    padding: 2%;
                    width: 100%;
                }

                section h2, section p{
                    background-color: white;
                    padding: 2%;
                    letter-spacing: 1px;
                }

                h2 + p{
                    margin-top: 1%;
                    margin-bottom: 1%;
                }

                .datum{
                    position: relative;
                    
                }

                .clanak-slike{
                    height:405px;
                    width:100%;  
                }

                img{
                    margin-bottom: 1%;
                }

                .datum > i{
                    padding: 1%;
                }

            </style>

        </head>

        <body>
            <header>

                <div id="ikone">
                    <i class="fa fa-facebook-square fa-2x"></i>
                    <i class="fa fa-twitter-square fa-2x"></i>
                    <i class="fa fa-instagram fa-2x"></i>
                    <i class="fa fa-youtube-square fa-2x"></i>
                    <i class="fa fa-envelope fa-2x"></i>
                </div>

                <div id="logo">
                    <div id="logo2">
                    <h5>PORTAL</h5>
                    </div>
                    <div id="logo3">
                        <p> NEWS PORTAL</p>
                    </div>
                </div>

                <nav>
                    <ul>
                        <li><a href="index.php">HOME</a></li>
                        <li><a href="kategorija.php">TRAVELS</a></li>
                        <li><a href="kategorija.php">CULTURE</a></li>
                        <li><a href="unos.php">ADD</a></li>
                        <li><a href="administracija.php">ADMINISTRATION</a></li>
                    </ul>
                </nav>
                <div style="clear: both;"></div>
            </header>

            <main>
                <section>
                        <h2> <?php echo $title; ?> </h2>          
                        <div class="datum"><p> <i class="fa fa-clock-o fa-lg"></i> <?php echo $fdate; ?> </p></div>

                        <figure class="clanak-slike">                         
                            <?php echo '<img src="img/' . $picture . '" height="405" width="100%">'; ?>
                        </figure> 
                        <p> <?php echo $about ; ?> </p>
                        <p> <?php echo $content; ?> </p>
                    
                </section>
            </main>

            <footer>
                <div id="podnozje">
                <p> Copyright 2020 NEWS PORTAL </p>
                <!--<p>kvidakovi@tvz.hr</p>-->
                </div>
            </footer>

        </body>

    </html> 

