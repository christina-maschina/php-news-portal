<!DOCTYPE html>
<html>

    <head>

        <title>TRAVELS/CULTURE</title>
        <meta charset="UTF-8" name="news portal" content="news from the world">
        <link rel="stylesheet" href="style.css" type="text/css">
        <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:ital,wght@1,700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Chelsea+Market&family=Sen&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
       

    </head>

    <body>
        <header >

            <div id="ikone">
                <i class="fa fa-facebook-square fa-2x"></i>
                <i class="fa fa-twitter-square fa-2x"></i>
                <i class="fa fa-instagram fa-2x"></i>
                <i class="fa fa-youtube-square fa-2x"></i>
                <i class="fa fa-envelope fa-2x"></i>
            </div>

            <div id="logo">
               <div id="logo2">
                   <h5>PORTAL</h5>
               </div>
               <div id="logo3">
                    <p> NEWS PORTAL</p>
               </div>
            </div>
            
            <nav>
                <ul>
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="kategorija.php?kategorija=travels">TRAVELS</a></li>
                    <li><a href="kategorija.php?kategorija=culture">CULTURE</a></li>
                    <li><a href="unos.php">ADD</a></li>
                    <li><a href="administracija.php">ADMINISTRATION</a></li>
                </ul>
            </nav>
            <div style="clear: both;"></div>
        </header>

        <main>

            <?php
                include 'connect.php';
                define ('UPLPATH', 'img/');

                $kategorija=$_GET['kategorija'];
                $query="SELECT *, DATE_FORMAT(datum, '%d.%m.%Y') AS fdatum FROM portal WHERE arhiva=0 AND kategorija='$kategorija' ORDER BY datum DESC, id DESC";
               
                $result=mysqli_query($dbc, $query);

            ?>

            <section style="height: inherit";>

                <h1><?php echo $kategorija; ?></h1> 
                <div class="border"></div>

                <?php
                    
                    while($row=mysqli_fetch_array($result)){

                        echo '<article> ';
                        echo '<a href="clanak.php?id=' . $row['id'] . '">';
                        echo '<figure class="index-slike">';
                        echo '<img src="' . UPLPATH . $row['slika'] . '" height="205" width="100%">';
                        echo '</figure>';
                        echo '<h2>' . $row['naslov'] . '</h2>';
                        echo '<p>' . $row['sazetak'] . '</p>';
                        echo '<div class="datum"><p> <i class="fa fa-clock-o fa-lg"></i>' . $row['fdatum'] . '</p></div>';
                        echo '</a>';
                        echo '</article>';
                    }
                ?>
                <div style="clear: both;"></div>
            </section>


        </main>

        <footer>
            <div id="podnozje">
            <p> Copyright 2020 NEWS PORTAL </p>
                <!--<p>kvidakovi@tvz.hr</p>-->
            </div>
        </footer>
    </body>

</html>