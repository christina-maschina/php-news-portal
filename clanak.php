<!DOCTYPE html>
<html>

    <head>

        <title>ARTICLE</title>
        <meta charset="UTF-8" name="news portal" content="news from the world">
        <link rel="stylesheet" href="style.css" type="text/css">
        <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:ital,wght@1,700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Chelsea+Market&family=Sen&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <style>

            body{
                background-color:red;
                background-image: linear-gradient(red 0%, #EA384D 61%, red 100%);
            }


            section{
                height: inherit;
                padding: 2%;
                width: 100%;
            }

            section h2, section p{
                background-color: white;
                padding: 2%;
                letter-spacing: 1px;
            }

            h2 + p{
                margin-top: 1%;
                margin-bottom: 1%;
            }

            .datum{
                position: relative;
                
            }
            

            img{
                margin-bottom: 1%;
            }
            

            .datum > i{
                padding: 1%;
            }

        </style>

    </head>

    <body>
        <header>

            <div id="ikone">
                <i class="fa fa-facebook-square fa-2x"></i>
                <i class="fa fa-twitter-square fa-2x"></i>
                <i class="fa fa-instagram fa-2x"></i>
                <i class="fa fa-youtube-square fa-2x"></i>
                <i class="fa fa-envelope fa-2x"></i>
            </div>

            <div id="logo">
               <div id="logo2">
                   <h5>PORTAL</h5>
               </div>
               <div id="logo3">
                    <p> NEWS PORTAL</p>
               </div>
            </div>

            <nav>
                <ul>
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="kategorija.php?kategorija=travels">TRAVELS</a></li>
                    <li><a href="kategorija.php?kategorija=culture">CULTURE</a></li>
                    <li><a href="unos.php">ADD</a></li>
                    <li><a href="administracija.php">ADMINISTRATION</a></li>
                </ul>
            </nav>
            <div style="clear: both;"></div>
        </header>

        <main>

            <?php
                    include 'connect.php';
                    define ('UPLPATH', 'img/');

                    $id=$_GET['id'];
                    $query="SELECT *, DATE_FORMAT(datum, '%d.%m.%Y') AS fdatum FROM portal WHERE id=$id";
                    $result=mysqli_query($dbc, $query);
                    $row=mysqli_fetch_array($result);
            ?>

            <section>

                <h2 id="h2_clanak"> <?php echo $row['naslov']; ?> </h2>
                    
                <div class="datum"><p> <i class="fa fa-clock-o fa-lg"></i> <?php echo $row['fdatum']; ?> </p></div>
                    <figure class="clanak-slike"> <?php echo '<img src="' . UPLPATH . $row['slika'] . '" height="405" width="100%">'; ?> </figure> 
                    <p> <?php echo $row['sazetak']; ?> </p>
                    <p> <?php echo $row['tekst']; ?> </p>
                
            </section>
        
        </main>
        
        <footer>
            <div id="podnozje">
            <p> Copyright 2020 NEWS PORTAL </p>
                <!--<p>kvidakovi@tvz.hr</p>-->
            </div>
        </footer>
    </body>

</html>