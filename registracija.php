<?php
include 'connect.php';
$registriranKorisnik = false; 
$msg='';
if(isset($_POST['prijava'])){

        $ime = $_POST['ime'];
        $prezime = $_POST['prezime']; 
        $username = $_POST['username']; 
        $lozinka = $_POST['pass']; 
        $hashed_password = password_hash($lozinka, CRYPT_BLOWFISH); 
        $razina = 0; 
        
        //Provjera postoji li u bazi već korisnik s tim korisničkim imenom
        $sql = "SELECT korisnicko_ime FROM korisnik WHERE korisnicko_ime = ?"; 
        $stmt = mysqli_stmt_init($dbc); 
        if (mysqli_stmt_prepare($stmt, $sql)) {    
            mysqli_stmt_bind_param($stmt, 's', $username);     
            mysqli_stmt_execute($stmt);     
            mysqli_stmt_store_result($stmt);    
            }  

        if(mysqli_stmt_num_rows($stmt) > 0){     
            $msg='Username already exists!';  
        } 

        else{ 
            // Ako ne postoji korisnik s tim korisničkim imenom - Registracija korisnika u bazi pazeći na SQL injection     
            $sql = "INSERT INTO korisnik (ime, prezime, korisnicko_ime, lozinka, razina) VALUES (?, ?, ?, ?, ?)";     
            $stmt = mysqli_stmt_init($dbc);     

            if (mysqli_stmt_prepare($stmt, $sql)) {         
                mysqli_stmt_bind_param($stmt, 'ssssd', $ime, $prezime, $username, $hashed_password, $razina);         
                mysqli_stmt_execute($stmt);         
                $registriranKorisnik = true;     
            } 
        
        }

    mysqli_close($dbc); 
}
?>

<!DOCTYPE html>
<html>

    <head>

        <title>REGISTRATION</title>
        <meta charset="UTF-8" name="news portal" content="news from the world">
        <link rel="stylesheet" href="style.css" type="text/css">
        <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:ital,wght@1,700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Chelsea+Market&family=Sen&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">

        <style>
            section{
                height: inherit;
                padding: 2%;
                width: 100%;
                margin: 0 auto;
            }
        </style>

    </head>

    <body>
        
        <header>

            <div id="ikone">
                <i class="fa fa-facebook-square fa-2x"></i>
                <i class="fa fa-twitter-square fa-2x"></i>
                <i class="fa fa-instagram fa-2x"></i>
                <i class="fa fa-youtube-square fa-2x"></i>
                <i class="fa fa-envelope fa-2x"></i>
            </div>

            <div id="logo">
               <div id="logo2">
                   <h5>PORTAL</h5>
               </div>
               <div id="logo3">
                    <p> NEWS PORTAL</p>
               </div>
            </div>

            <nav>
                <ul>
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="kategorija.php?kategorija=travels">TRAVELS</a></li>
                    <li><a href="kategorija.php?kategorija=culture">CULTURE</a></li>
                    <li><a href="unos.php">ADD</a></li>
                    <li><a href="administracija.php">ADMINISTRATION</a></li>
                </ul>
            </nav>
            <div style="clear: both;"></div>
        </header>

        <main>

            <section>

                <?php               
                    //Registracija je prošla uspješno  
                    if($registriranKorisnik == true) {                     
                        echo '<p class="login_msg">The user has been successfully registered!</p>';                 
                        } 
                        else {        
                        //registracija nije protekla uspješno ili je korisnik prvi put došao na stranicu             
                ?>             
                            <section role="main">              
                                <form enctype="multipart/form-data" action="" method="POST">                  
                                    <div class="form-item">                     
                                        <span id="porukaIme" class="bojaPoruke"></span>                     
                                        <label for="title">Name: </label>                     
                                        <div class="form-field">                     
                                            <input type="text" name="ime" id="ime" class="formfield-textual">                 
                                        </div>                 
                                    </div>                 
                                    <div class="form-item">                     
                                        <span id="porukaPrezime" class="bojaPoruke"></span>                     
                                        <label for="about">Last name: </label>                     
                                        <div class="form-field">                     
                                            <input type="text" name="prezime" id="prezime" class="formfield-textual">                 
                                        </div>                 
                                    </div>                 
                                    <div class="form-item">                     
                                        <span id="porukaUsername" class="bojaPoruke"></span>                                          
                                        <label for="content">Username:</label>         
                                        <!-- Ispis poruke nakon provjere korisničkog imena u bazi -->                     
                                        <?php echo '<br><span class="bojaPoruke">'.$msg.'</span>'; ?>                     
                                        <div class="form-field">                     
                                            <input type="text" name="username" id="username" class="formfield-textual">                 
                                        </div>                 
                                    </div>                 
                                    <div class="form-item">                     
                                        <span id="porukaPass" class="bojaPoruke"></span>                     
                                        <label for="pass">Password: </label>                     
                                        <div class="form-field"> 
                                            <input type="password" name="pass" id="pass" class="formfield-textual">                  
                                        </div>                  
                                    </div>                 
                                    <div class="form-item">                     
                                        <span id="porukaPassRep" class="bojaPoruke"></span>                     
                                        <label for="pass2">Repeat password: </label>                     
                                        <div class="form-field">                     
                                            <input type="password" name="passRep" id="passRep" class="formfield-textual">                 
                                        </div>                 
                                    </div>                                  
                                    <div class="form-item">                     
                                        <button type="submit" value="Prijava" name="prijava" id="slanje"><span>Submit</span></button>                 
                                    </div> 
                                </form>                        
                            </section>          
                <?php 
                    }
                 ?> 

            </section>                   
        </main>

        <script type="text/javascript"> 
            //javascript validacija forme   
            document.getElementById("slanje").onclick = function(event) {                      
                var slanjeForme = true;                      
                
                // Ime korisnika mora biti uneseno                 
                var poljeIme = document.getElementById("ime");                 
                var ime = document.getElementById("ime").value;                 
                
                if (ime.length == 0) {                     
                    slanjeForme = false;                     
                    poljeIme.style.border="1px solid red";                     
                    document.getElementById("porukaIme").innerHTML="<br>Name is missing<br>";                 
                    } else {                     
                        poljeIme.style.border="1px solid green";                     
                        document.getElementById("porukaIme").innerHTML="";                 
                        } 

                // Prezime korisnika mora biti uneseno                 
                var poljePrezime = document.getElementById("prezime");                 
                var prezime = document.getElementById("prezime").value;

                if (prezime.length == 0) {                     
                    slanjeForme = false; 
                    poljePrezime.style.border="1px solid red";                     
                    document.getElementById("porukaPrezime").innerHTML="<br>Last name is missing<br>";                 
                    } else {                     
                        poljePrezime.style.border="1px solid green";                     
                        document.getElementById("porukaPrezime").innerHTML="";                 
                        }                      
                        
                // Korisničko ime mora biti uneseno                 
                var poljeUsername = document.getElementById("username");                 
                var username = document.getElementById("username").value;                 
                
                if (username.length == 0) {                     
                    slanjeForme = false;                     
                    poljeUsername.style.border="1px solid red";                     
                    document.getElementById("porukaUsername").innerHTML="<br>Username is missing<br>";                 
                    } else {                     
                        poljeUsername.style.border="1px solid green";                     
                        document.getElementById("porukaUsername").innerHTML="";                 
                        }                     
                        
                // Provjera podudaranja lozinki                 
                var poljePass = document.getElementById("pass");                
                var pass = document.getElementById("pass").value;                 
                var poljePassRep = document.getElementById("passRep");                 
                var passRep = document.getElementById("passRep").value;                 
                
                if (pass.length == 0 || passRep.length == 0 || pass != passRep) {                     
                    slanjeForme = false;                     
                    poljePass.style.border="1px solid red";                     
                    poljePassRep.style.border="1px solid red";                     
                    document.getElementById("porukaPass").innerHTML="<br>Passwords are not the same<br>";                     
                    document.getElementById("porukaPassRep").innerHTML="<br>Passwords are not the same<br>";                 
                    } else {                     
                        poljePass.style.border="1px solid green";                     
                        poljePassRep.style.border="1px solid green";                     
                        document.getElementById("porukaPass").innerHTML="";                     
                        document.getElementById("porukaPassRep").innerHTML="";                 
                        }                      
                        
                if (slanjeForme != true) {                     
                    event.preventDefault();                 
                    } 

            };              

        </script>   
    <footer>
        <div id="podnozje">
        <p> Copyright 2020 NEWS PORTAL </p>
                <!--<p>kvidakovi@tvz.hr</p>-->
        </div>
    </footer>

</body>
</html>
